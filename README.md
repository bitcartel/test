usage

import { bsv } from 'https://bitbucket.org/bitcartel/test/mod.ts"

example

import { bsv } from 'https://bitbucket.org/bitcartel/test/mod.ts"
import { Buffer } from 'https://cdn.pika.dev/buffer';

var str = "this is my string"
var base58 = bsv.encoding.Base58.fromBuffer(Buffer.from(str)).toString()
console.log(base58)


# notes

reference

https://github.com/denoland/deno/issues/4979
